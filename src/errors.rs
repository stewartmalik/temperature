#[derive(Debug, Eq, PartialEq)]
pub enum TemperatureError {
    CouldNotReadSensorData(String),
    CouldNotSubmitToDb(String),
}

impl From<std::io::Error> for TemperatureError {
    fn from(error: std::io::Error) -> TemperatureError {
        TemperatureError::CouldNotReadSensorData(error.to_string())
    }
}

impl From<reqwest::Error> for TemperatureError {
    fn from(error: reqwest::Error) -> TemperatureError {
        TemperatureError::CouldNotSubmitToDb(error.to_string())
    }
}