extern crate reqwest;

use crate::sensor::Temperature;
use crate::errors::TemperatureError;

const MEASUREMENT: &str = "temperature";
const TAG_COLLECTOR_KEY: &str = "collector";
const TAG_COLLECTOR_VALUE: &str = "raspberrypi-1";

pub struct InfluxClient {
    influx_db: String,
    influx_host: String,
    influx_port: String,
    measurement: String,
    tag_collector: (String, String),
}

impl InfluxClient {
    pub fn with_config(influx_db: &str, influx_host: &str, influx_port: &str,
                                        measurement: Option<&str>, tag_collector: Option<(&str, &str)>) -> InfluxClient {
        let measurement = measurement.unwrap_or(MEASUREMENT);
        let tag_collector = tag_collector.unwrap_or((TAG_COLLECTOR_KEY, TAG_COLLECTOR_VALUE));

        InfluxClient {
            influx_db: influx_db.into(),
            influx_host: influx_host.into(),
            influx_port: influx_port.into(),
            measurement: measurement.into(),
            tag_collector: (tag_collector.0.into(), tag_collector.1.into()),
        }
    }

    pub fn add_temperature_to_db(self: &Self, temp: Temperature) -> Result<(), TemperatureError> {
        let url = format!("http://{}:{}/write?db={}", self.influx_host, self.influx_port, self.influx_db);
        let insert = format!("{},{}={} value={}", self.measurement, self.tag_collector.0, self.tag_collector.1, temp);

        let client = reqwest::Client::new();
        let _response = client.post(&url).body(insert).send()?;

        Ok(())
    }
}
