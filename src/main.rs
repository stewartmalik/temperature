mod errors;
mod sensor;
mod influx_client;

use clap::{App, load_yaml};
use std::path::{Path};

fn main() {
    let yaml = load_yaml!("options.yaml");
    let options = App::from_yaml(yaml).get_matches();

    // Convert to Path
    let sensor_path = Path::new(options.value_of("SENSOR_PATH").unwrap());

    // Load the data from the file
    let file_data = sensor::read_sensor_data(sensor_path)
        .unwrap_or_else(|e| {
            eprintln!("{:?}", e);
            "".to_string()
        });

    // Now let's parse that data and attempt to extract the temperature
    let s_temp = sensor::parse_sensor_data(&file_data)
        .unwrap_or_else(|e| {
            eprintln!("{:?}", e);
            "".to_string()
        });

    // See if we can convert it to a number
    // The default to return if parsing fails is 0.0
    let f_temp = sensor::convert_temperature(&s_temp);

    // Print it
    println!("Temperature: {} ℃", f_temp);

    // Create influx client and submit it
    let influx_client = influx_client::InfluxClient::with_config(
        options.value_of("influx_db").unwrap(),
        options.value_of("influx_host").unwrap(),
        options.value_of("influx_port").unwrap(),
        None,
        None
    );
    influx_client.add_temperature_to_db(f_temp)
        .unwrap_or_else(|e| eprintln!("{:?}", e));
}